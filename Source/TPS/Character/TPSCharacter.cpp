// Copyright Epic Games, Inc. All Rights Reserved.
#include "TPSCharacter.h"
#include "PlayerStatComponent.h"
#include "TPS/FuncLibrary/Types.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "TPS/Game/TPSGameInstance.h"
#include "Animation/AnimInstanceProxy.h"

ATPSCharacter::ATPSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	PlayerStatsComp = CreateDefaultSubobject<UPlayerStatComponent>("PlayerStatComponent");

	AimEnabled = false;

	maxStamina = 30.0f;
	currentStamina = 5.0f;
	staminaSprintUsageRate = 2.0f;
	staminaRechargeRate = 1.0f;

	canStaminaRecharge = true;
	delayForStaminaRecharge = 0.3f;
}

void ATPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	InitWeapon(InitWeaponName);

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void ATPSCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (SprintRunEnabled)
	{
		currentStamina = FMath::FInterpConstantTo(currentStamina, 0.0f, DeltaSeconds, staminaSprintUsageRate);

		if (currentStamina <= 0.0f)
		{
			DepletedAllStamina();
		}
	}
	else
	{
		if (currentStamina < maxStamina)
		{
			if (canStaminaRecharge)
			{
				currentStamina = FMath::FInterpConstantTo(currentStamina, maxStamina, DeltaSeconds, staminaRechargeRate);
			}
		}
	}
	
	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);
}

void ATPSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);
	check(NewInputComponent)

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATPSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATPSCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("Sprint"), IE_Pressed, this, &ATPSCharacter::StartSprint);
	NewInputComponent->BindAction(TEXT("Sprint"), IE_Released, this, &ATPSCharacter::StopSprint);

	NewInputComponent->BindAction(TEXT("Walk"), IE_Pressed, this, &ATPSCharacter::Walk);
	NewInputComponent->BindAction(TEXT("Walk"), IE_Released, this, &ATPSCharacter::StopWalk);

	NewInputComponent->BindAction(TEXT("Aim"), IE_Pressed, this, &ATPSCharacter::AimPressed);
	NewInputComponent->BindAction(TEXT("Aim"), IE_Released, this, &ATPSCharacter::AimReleased);

	NewInputComponent->BindAction(TEXT("Fire"), EInputEvent::IE_Pressed, this, &ATPSCharacter::AttackPressed);
	NewInputComponent->BindAction(TEXT("Fire"), EInputEvent::IE_Released, this, &ATPSCharacter::AttackReleased);
}

void ATPSCharacter::EnableStaminaGain()
{
	canStaminaRecharge = true;
}

void ATPSCharacter::DepletedAllStamina()
{
	StopSprint();
}

void ATPSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATPSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATPSCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (myController, SprintRunEnabled, AimEnabled)
	{
		FHitResult ResultHit;
		//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
		myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

		float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
		SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));

		if (SprintRunEnabled)
		{

			//FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
			//FRotator myRotator = myRotationVector.ToOrientationRotator();
			//SetActorRotation((FQuat(myRotator)));	
		}
	}
}

void ATPSCharacter::CharacterUpdate()
{
	float ResSpeed = 500.0f;

	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementInfo.SprintRunSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimWalkSpeed;
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATPSCharacter::ChangeMovementState()
{
	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		MovementState = EMovementState::Run_State;
	}
	else
	{
		if (SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			RunEnabled = false;
			MovementState = EMovementState::SprintRun_State;
		}
		else
		{
			if (WalkEnabled && !RunEnabled && !AimEnabled && !SprintRunEnabled)
			{
				MovementState = EMovementState::Walk_State;
			}
			else
			{
				if (!WalkEnabled && !RunEnabled && !SprintRunEnabled && AimEnabled)
				{
					MovementState = EMovementState::Aim_State;
				}
			}
		}
	}
	CharacterUpdate();

	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

void ATPSCharacter::AimPressed()
{
	AimEnabled = true;
}

void ATPSCharacter::AimReleased()
{
	AimEnabled = false;
}

void ATPSCharacter::AttackPressed()
{
	AttackCharEvent(true);
}

void ATPSCharacter::AttackReleased()
{
	AttackCharEvent(false);
}

void ATPSCharacter::StartSprint()
{
	if (currentStamina > 0.0f)
	{
		UE_LOG(LogTemp, Warning, (TEXT("We are now sprinting")));
		SprintRunEnabled = true;

		canStaminaRecharge = false;
		GetWorld()->GetTimerManager().ClearTimer(staminaRechargeTimerHandle);
	}
}

void ATPSCharacter::StopSprint()
{
	if (currentStamina <= 0.0f)
	{
		UE_LOG(LogTemp, Warning, (TEXT("We have stopped sprinting")));
		SprintRunEnabled = false;
		GetCharacterMovement()->MaxWalkSpeed = 500.0f;

		GetWorld()->GetTimerManager().SetTimer(staminaRechargeTimerHandle, this, &ATPSCharacter::EnableStaminaGain, delayForStaminaRecharge, false);
	}
}

void ATPSCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp, Warning, (TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL")));
	}	
}

AWeaponDefault* ATPSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATPSCharacter::InitWeapon(FName IdWeaponName)
{
	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;
					//Remove !!! Debug
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATPSCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATPSCharacter::WeaponReloadEnd);
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, (TEXT("ATPSCharacter::InitWeapon - Weapon not found in table -NULL")));
		}
	}
}

void ATPSCharacter::TryRealodWeapon()
{
	if (CurrentWeapon)
	{
		if (CurrentWeapon->GetWeaponRound() <= CurrentWeapon->WeaponSetting.MaxRound)
		{
			CurrentWeapon->InitReload();
		}	
	}
}

void ATPSCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	//WeaponReloadStart_BP(Anim);
}

void ATPSCharacter::WeaponReloadEnd()
{
	//WeaponReloadEnd_BP();
}

void ATPSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{

}

void ATPSCharacter::WeaponReloadEnd_BP_Implementation()
{

}

UDecalComponent* ATPSCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void ATPSCharacter::Walk()
{
	UE_LOG(LogTemp, Warning, (TEXT("We are now walking")));
	bIsWalking = true;
}

void ATPSCharacter::StopWalk()
{
	UE_LOG(LogTemp, Warning, (TEXT("We have stopped walking")));
	bIsWalking = false;
}