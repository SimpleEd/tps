// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlayerStatComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnStaminaChanged,UPlayerStatComponent*, OwningComp, float, NewStamina, float, Tired);
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UPlayerStatComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerStatComponent();

protected:
	virtual void BeginPlay() override;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StatComp|Stamina")
		float MaxStamina;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "StatComp|Stamina")
		float UpdateTimeStamina = 1.0f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "StatComp|Stamina")
		float UpdateDelayStamina = 1.0f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "StatComp|Stamina")
		float ModifierStamina = 1.0f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "StatComp|Stamina")
		float Tired = 0.1f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Attributes|Stamina")
		bool AutoTired = true;
	
	
	UPROPERTY(BlueprintAssignable)
		FOnStaminaChanged OnStaminaChanged;
	
	UPROPERTY()
	float Stamina;
	
public:
	UFUNCTION(BlueprintCallable)
	float GetStamina() const;

	UFUNCTION(BlueprintCallable)
	void LowerStamina(float Value);
	
	UFUNCTION()
	void ChangeStamina(float NewTired);

	UFUNCTION()
	float GetTired() const { return Tired; }
	
	UFUNCTION()
	bool IsNoTired();

	void ControlSprintingTimer(bool bIsSprinting) const;
private:
	FTimerHandle StaminaTimerHandle;
	
	void StaminaUpdate();
	
	void RegenStamina();

};
