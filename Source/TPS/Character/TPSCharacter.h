// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPS/Weapons/WeaponDefault.h"
#include "TPSCharacter.generated.h"

class UPlayerStatComponent;

UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATPSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UPlayerStatComponent* PlayerStatsComp;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

public:
		// Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;

		// Weapon
	AWeaponDefault* CurrentWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

		// Movement
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
			FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		bool RunEnabled = false;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnabled = false;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		bool AimWalkEnabled = false;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		bool SprintRunEnabled = false;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		bool bIsSprinting = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movements")
		bool IsSprinting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movements")
		bool bIsWalking;

		// Stamina Stats
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float currentStamina;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float maxStamina;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float staminaSprintUsageRate;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float staminaRechargeRate;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Stamina")
		float delayForStaminaRecharge;
	UPROPERTY(Editanywhere, BlueprintReadWrite, Category = "Stamina")
		bool canStaminaRecharge;

	UFUNCTION(BlueprintCallable)
		void EnableStaminaGain();

	UFUNCTION(BlueprintCallable)
		void DepletedAllStamina();

public:
		// Input
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);
	
	float AxisX = 0.0f;
	float AxisY = 0.0f;

		// Movement Func
	UFUNCTION()
		void MovementTick(float DeltaTime);

	UFUNCTION(BlueprintCallable, Category = "Creature|Movement")
		void StartSprint();
	UFUNCTION(BlueprintCallable, Category = "Creature|Movement")
		void StopSprint();
	UFUNCTION(BlueprintCallable, Category = "Creature|Movement")
		void Walk();
	UFUNCTION(BlueprintCallable, Category = "Creature|Movement")
		void StopWalk();
	UFUNCTION(BlueprintCallable, Category = "Creature|Movement")
		void AimPressed();
	UFUNCTION(BlueprintCallable, Category = "Creature|Movement")
		void AimReleased();
	UFUNCTION(BlueprintCallable, Category = "Creature|Movement")
		void AttackPressed();
	UFUNCTION(BlueprintCallable, Category = "Creature|Movement")
		void AttackReleased();

	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();

	FTimerHandle SprintingHandle;
	FTimerHandle staminaRechargeTimerHandle;

		// For Demo
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;

	UDecalComponent* CurrentCursor = nullptr;

public: 
		// Weapon Func
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);

	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();

	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName);

	UFUNCTION(BlueprintCallable)
		void TryRealodWeapon();

	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);

	UFUNCTION()
		void WeaponReloadEnd();

	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP();

	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

protected:

	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float Direction = 0.0f;
};


