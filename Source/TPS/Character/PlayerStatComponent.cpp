// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/Character/PlayerStatComponent.h"
#include "PhysicsSettingsEnums.h"
#include "TPSCharacter.h"
#include "Net/UnrealNetwork.h"
#include "TimerManager.h"
#include "Engine/Engine.h"


// Sets default values for this component's properties
UPlayerStatComponent::UPlayerStatComponent()
{
	MaxStamina = 100.0f;
	Stamina = 0.0f;
	
}
// Called when the game starts
void UPlayerStatComponent::BeginPlay()
{
	Super::BeginPlay();
	
	Stamina = MaxStamina;
	OnStaminaChanged.Broadcast(this, Stamina, MaxStamina);
	GetWorld()->GetTimerManager().SetTimer(StaminaTimerHandle, this, &UPlayerStatComponent::RegenStamina, 1.0f, true);
}

float UPlayerStatComponent::GetStamina() const
{
	return Stamina;
}

void UPlayerStatComponent::ChangeStamina(float NewTired)
{
	if(NewTired < 0.0f && GetWorld())
	{
		return;
	}
	Stamina = FMath::Clamp(Stamina - NewTired, 0.0f, MaxStamina);
	OnStaminaChanged.Broadcast(this,Stamina,NewTired);
		if(Stamina == MaxStamina && GetWorld())
		{
			GetWorld()->GetTimerManager().SetTimer(StaminaTimerHandle,
				this,&UPlayerStatComponent::StaminaUpdate,
				UpdateTimeStamina,
				true,
				UpdateDelayStamina);
		}
}	

bool UPlayerStatComponent::IsNoTired()
{
	return GetStamina() > 0.0f;
}

void UPlayerStatComponent::StaminaUpdate()
{
	Stamina = FMath::Min(Stamina + ModifierStamina, MaxStamina);
	OnStaminaChanged.Broadcast(this, Stamina, ModifierStamina);
		if(Stamina == MaxStamina && GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(StaminaTimerHandle);
		}


}

void UPlayerStatComponent::LowerStamina(float Value)
{
		Stamina -= Value;
}

void UPlayerStatComponent::RegenStamina()
{
	if(Stamina >= 100.0f)
		Stamina = 100.0f;
	else
	{
		++Stamina;
	}
}

void UPlayerStatComponent::ControlSprintingTimer(bool bIsSprinting) const
{
	if(bIsSprinting)
	{
		GetWorld()->GetTimerManager().PauseTimer(StaminaTimerHandle);
	}
	else
	{
		GetWorld()->GetTimerManager().UnPauseTimer(StaminaTimerHandle);
	}
}