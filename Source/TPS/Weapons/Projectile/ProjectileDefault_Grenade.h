#pragma once

#include "TPS/Weapons/Projectile/ProjectileDefault.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProjectileDefault_Grenade.generated.h"

UCLASS()
class TPS_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()
	
public:
	
	AProjectileDefault_Grenade();

protected:
	
	virtual void BeginPlay() override;

public:	

	virtual void Tick(float DeltaTime) override;

	void TimerExplose(float DeltaTime);

	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	
	virtual void ImpactProjectile() override;


	void Explose();

	
	bool TimerEnabled = false;
	float TimerToExplose = 0.0f;
	float TimeToExplose = 5.0f;
};
